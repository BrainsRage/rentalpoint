"use strict";
var router_1 = require("@angular/router");
var Homecomponent = require("./home/home.component");
var appRoutes = [
    { path: "", redirectTo: "home", pathMatch: "full" },
    { path: "home", component: Homecomponent.HomeComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routes.js.map