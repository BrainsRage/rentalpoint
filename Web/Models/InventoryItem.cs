﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    [Table("InventoryItems")]
    public class InventoryItem
    {
        public int Id { get; set; }
        public float Price { get; set; }
        public string Title { get; set; }

        public int InventoryTypeId { get; set; }
        public InventoryType InventoryType { get; set; }
    }
}
