﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    [Table("InventoryTypes")]
    public class InventoryType
    {
        public int Id { get; set; }
        public string Title { get; set; }   
    }
}
