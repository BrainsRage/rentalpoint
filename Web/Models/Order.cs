﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    [Table("Orders")]
    public class Order
    {
        public int Id { get; set; }
        public float Amount { get; set; }
        public DateTime DateIssue { get; set; }
        public DateTime? DateReturn { get; set; }
        public DateTime? DatePay { get; set; }

        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        [NotMapped]
        public string Items { get; set; }

        [NotMapped]
        public List<OrderInventoryItem> OrderInventoryItems { get; set; }

        /* public List<InventoryItem> Items { get; set
             //return OrderInventoryItems.Select(x => x.InventoryItem).ToList();
         }*/

        public List<OrderInventoryItem> GetLinkWithItems(List<InventoryItem> items, Order order, int customerId)
        {
            var links = new List<OrderInventoryItem>();
            foreach (var item in items)
            {
                links.Add(new OrderInventoryItem
                {
                    CustomerId = customerId,
                    OrderId = order.Id,
                    InventoryItemId = item.Id
                });
            }
            return links;
        }
    }
}