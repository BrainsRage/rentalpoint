﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Web.Common.DB;

namespace Web.Migrations
{
    [DbContext(typeof(MySqlContext))]
    [Migration("20161014065331_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1");

            modelBuilder.Entity("Web.Models.Customer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateRegistration");

                    b.Property<string>("Name");

                    b.Property<string>("Patronymic");

                    b.Property<string>("Phone");

                    b.Property<string>("Surname");

                    b.HasKey("Id");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("Web.Models.InventoryItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("InventoryTypeId");

                    b.Property<float>("Price");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("InventoryTypeId");

                    b.ToTable("InventoryItems");
                });

            modelBuilder.Entity("Web.Models.InventoryType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("InventoryTypes");
                });

            modelBuilder.Entity("Web.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<float>("Amount");

                    b.Property<int>("CustomerId");

                    b.Property<DateTime>("DateIssue");

                    b.Property<DateTime?>("DatePay");

                    b.Property<DateTime?>("DateReturn");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("Web.Models.OrderInventoryItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CustomerId");

                    b.Property<int>("InventoryItemId");

                    b.Property<int>("OrderId");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("InventoryItemId");

                    b.HasIndex("OrderId");

                    b.ToTable("OrderInventoryItems");
                });

            modelBuilder.Entity("Web.Models.InventoryItem", b =>
                {
                    b.HasOne("Web.Models.InventoryType", "InventoryType")
                        .WithMany()
                        .HasForeignKey("InventoryTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Web.Models.Order", b =>
                {
                    b.HasOne("Web.Models.Customer", "Customer")
                        .WithMany()
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Web.Models.OrderInventoryItem", b =>
                {
                    b.HasOne("Web.Models.Customer", "Customer")
                        .WithMany()
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Web.Models.InventoryItem", "InventoryItem")
                        .WithMany()
                        .HasForeignKey("InventoryItemId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Web.Models.Order", "Order")
                        .WithMany()
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
