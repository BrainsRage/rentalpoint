﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.Common.DB;

namespace Web.Controllers
{
    public abstract class ApiController : Controller
    {
        protected MySqlContext Context;

        protected ApiController(MySqlContext context)
        {
            this.Context = context;
        }
    }
}