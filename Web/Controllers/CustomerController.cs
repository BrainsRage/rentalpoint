﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.Common.DB;
using Web.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : ApiController
    {
        // GET: api/values
        public CustomerController(MySqlContext context) : base(context)
        {
        }

        [HttpGet]
        public IEnumerable<Customer> Get()
        {
            return Context.Customers.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Customer Get(int id)
        {
          /*  var order =
                Context.Orders.Where(x => x.Id == id)
                    .Include(x => x.OrderInventoryItems)
                    .ThenInclude(x => x.InventoryItem)
                    .FirstOrDefault();
            var items = order.Items;*/
            return Context.Customers.FirstOrDefault(x => x.Id == id);
        }

        // POST api/values
        [HttpPost]
        public Customer Post([FromBody] Customer customer)
        {
            customer.DateRegistration = DateTime.Now;
            Context.Customers.Add(customer);
            Context.SaveChanges();
            return customer;
        }

        // PUT api/values/5
        [HttpPut]
        public bool Put([FromBody] Customer value)
        {
            Context.Customers.Update(value);
            try
            {
                Context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            var customer = Context.Customers.First(x => x.Id == id);
            Context.Customers.Remove(customer);
            try
            {
                Context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}