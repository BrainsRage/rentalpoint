﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using Web.Common.DB;
using Web.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class InventoryTypeController : ApiController
    {
        // GET: api/values
        public InventoryTypeController(MySqlContext context) : base(context)
        {
        }

        [HttpGet]
        public IEnumerable<InventoryType> Get()
        {
            return Context.InventoryTypes.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public InventoryType Get(int id)
        {
            return Context.InventoryTypes.FirstOrDefault(x => x.Id == id);
        }

        // POST api/values
        [HttpPost]
        public InventoryType Post([FromBody] InventoryType value)
        {
            var type = new InventoryType();
            type.Title = value.Title;
            Context.InventoryTypes.Add(type);
            Context.SaveChanges();
            return type;
        }

        // PUT api/values/5
        [HttpPut]
        public bool Put([FromBody] InventoryType value)
        {
            Context.InventoryTypes.Update(value);
            try
            {
                Context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            var type = Context.InventoryTypes.First(x => x.Id == id);
            Context.InventoryTypes.Remove(type);
            try
            {               
                Context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;    
        }
    }
}