﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Web.Common.DB;
using Web.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class InventoryItemController : ApiController
    {
        public InventoryItemController(MySqlContext context) : base(context)
        {
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<InventoryItem> Get()
        {
            return Context.InventoryItems.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public InventoryItem Get(int id)
        {
            return Context.InventoryItems.FirstOrDefault(x => x.Id == id);
        }

        // POST api/values
        [HttpPost]
        public InventoryItem Post([FromBody] InventoryItem item)
        {
            Context.InventoryItems.Add(item);
            Context.SaveChanges();
            return item;
        }

        // PUT api/values/5
        [HttpPut]
        public bool Put([FromBody] InventoryItem value)
        {
            Context.InventoryItems.Update(value);
            try
            {
                Context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            var item = Context.InventoryItems.First(x => x.Id == id);
            Context.InventoryItems.Remove(item);
            try
            {
                Context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}