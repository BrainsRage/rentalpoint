﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Web.Common.DB;
using Web.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : ApiController
    {
        public OrderController(MySqlContext context) : base(context)
        {
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Order> Get()
        {
            return Context.Orders.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Order Get(int id)
        {
            return Context.Orders.FirstOrDefault(x => x.Id == id);
        }

        // POST api/values
        [HttpPost]
        public Order Post([FromBody] Order order)
        {
            order.DateIssue = DateTime.Now;
            order.DatePay = null;
            order.DateReturn = null;
            var items = JsonConvert.DeserializeObject<List<InventoryItem>>(order.Items);

            Context.Orders.Add(order);
            Context.SaveChanges();

            var links =order.GetLinkWithItems(items, order, order.CustomerId);
            Context.AddRange(links);
            Context.SaveChanges();

            return order;
        }

        // PUT api/values/5
        [HttpPut]
        public bool Put([FromBody] Order value)
        {
            Context.Orders.Update(value);
            try
            {
                Context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            var order = Context.Orders.First(x => x.Id == id);
            Context.Orders.Remove(order);
            try
            {
                Context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}