﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Web.Models;

namespace Web.Common.DB
{
    public class MySqlContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; }
        public DbSet<InventoryType> InventoryTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderInventoryItem> OrderInventoryItems { get; set; }

        public MySqlContext(DbContextOptions<MySqlContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .ToTable("Customers");
            modelBuilder.Entity<InventoryItem>()
                .ToTable("InventoryItems");
            modelBuilder.Entity<InventoryType>()
                .ToTable("InventoryTypes");
            modelBuilder.Entity<Order>()
                .ToTable("Orders");
            modelBuilder.Entity<OrderInventoryItem>()
                .ToTable("OrderInventoryItems");
        }
    }
}