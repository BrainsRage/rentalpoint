﻿import {Routes, RouterModule} from "@angular/router";
import {InventoryTypeListComponent} from "./inventory-type/inventory-type-list.component";
import {InventoryItemListComponent} from "./inventory-item/inventory-item-list.component";
import {HomeComponent} from "./home/home.component";
import {CustomerListComponent} from "./customer/customer-list.component";
import {OrderListComponent} from "./order/order-list.component";

const appRoutes: Routes = [
    { path: "", component: HomeComponent, pathMatch: "full" },
    { path: "inventory-types", component: InventoryTypeListComponent },
    { path: "inventory-items", component: InventoryItemListComponent },
    { path: "customers", component: CustomerListComponent },
    { path: "orders", component: OrderListComponent }
];

export const routing = RouterModule.forRoot(appRoutes);