﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../models/customer.model';
import { CustomerService } from '../services/customer.service';

@Component({
    moduleId: module.id,
    selector: 'customers',
    templateUrl: '/app/views/customer/list.html'
})
export class CustomerListComponent {
    customers: Customer[];

    constructor(
        private customersService: CustomerService,
        private router: Router
    ) {
        this.customers = [];
    }

    getCustomers(): void {
        this.customersService.getAll().then(customers => this.customers = customers);
    }

    add(
        name: string,
        surname: string,
        patronymic: string,
        phone: string
    ): void {
        name = name.trim();
        surname = surname.trim();
        patronymic = patronymic.trim();
        if (!name || !surname || !patronymic) {
            alert('Не все поля заполнены');
            return;
        }
        const customer = new Customer(0, name, surname, patronymic, phone);
        this.customersService.create(customer)
            .then(customer => {
                this.customers.push(customer);
            });
    }

    delete(customer: Customer): void {
        this.customersService
            .delete(customer.id)
            .then(() => {
                this.customers = this.customers.filter(h => h !== customer);
            });
    }

    ngOnInit(): void {
        this.getCustomers();
    }
}