"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var customer_model_1 = require('../models/customer.model');
var customer_service_1 = require('../services/customer.service');
var CustomerListComponent = (function () {
    function CustomerListComponent(customersService, router) {
        this.customersService = customersService;
        this.router = router;
        this.customers = [];
    }
    CustomerListComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customersService.getAll().then(function (customers) { return _this.customers = customers; });
    };
    CustomerListComponent.prototype.add = function (name, surname, patronymic, phone) {
        var _this = this;
        name = name.trim();
        surname = surname.trim();
        patronymic = patronymic.trim();
        if (!name || !surname || !patronymic) {
            alert('Не все поля заполнены');
            return;
        }
        var customer = new customer_model_1.Customer(0, name, surname, patronymic, phone);
        this.customersService.create(customer)
            .then(function (customer) {
            _this.customers.push(customer);
        });
    };
    CustomerListComponent.prototype.delete = function (customer) {
        var _this = this;
        this.customersService
            .delete(customer.id)
            .then(function () {
            _this.customers = _this.customers.filter(function (h) { return h !== customer; });
        });
    };
    CustomerListComponent.prototype.ngOnInit = function () {
        this.getCustomers();
    };
    CustomerListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'customers',
            templateUrl: '/app/views/customer/list.html'
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, router_1.Router])
    ], CustomerListComponent);
    return CustomerListComponent;
}());
exports.CustomerListComponent = CustomerListComponent;
//# sourceMappingURL=customer-list.component.js.map