﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InventoryType } from '../models/inventory-type.model';
import { InventoryTypeService } from '../services/inventory-type.service';

@Component({
    moduleId: module.id,
    selector: 'inventory-type-list',
    templateUrl: '/app/views/inventory-type/list.html'
})
export class InventoryTypeListComponent {
    typesList: InventoryType[];

    constructor(
        private typesService: InventoryTypeService,
        private router: Router
    ) {
    }

    getTypesList(): void {
        this.typesService.getAll()
            .then(typesList => {
                this.typesList = typesList;
                console.log(typesList);
            });
    }

    add(title: string): void {
        title = title.trim();
        if (!title) {
            return;
        }
        var type = new InventoryType(0, title);
        this.typesService.create(type)
            .then((type) => {
                this.typesList.push((type));
            });
    }

    delete(type: InventoryType): void {
        this.typesService
            .delete(type.id)
            .then(() => {
                this.typesList = this.typesList.filter(h => h !== type);
            });
    }

    ngOnInit(): void {
        this.getTypesList();
    }
}