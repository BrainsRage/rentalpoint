"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var inventory_type_model_1 = require('../models/inventory-type.model');
var inventory_type_service_1 = require('../services/inventory-type.service');
var InventoryTypeListComponent = (function () {
    function InventoryTypeListComponent(typesService, router) {
        this.typesService = typesService;
        this.router = router;
    }
    InventoryTypeListComponent.prototype.getTypesList = function () {
        var _this = this;
        this.typesService.getAll()
            .then(function (typesList) {
            _this.typesList = typesList;
            console.log(typesList);
        });
    };
    InventoryTypeListComponent.prototype.add = function (title) {
        var _this = this;
        title = title.trim();
        if (!title) {
            return;
        }
        var type = new inventory_type_model_1.InventoryType(0, title);
        this.typesService.create(type)
            .then(function (type) {
            _this.typesList.push((type));
        });
    };
    InventoryTypeListComponent.prototype.delete = function (type) {
        var _this = this;
        this.typesService
            .delete(type.id)
            .then(function () {
            _this.typesList = _this.typesList.filter(function (h) { return h !== type; });
        });
    };
    InventoryTypeListComponent.prototype.ngOnInit = function () {
        this.getTypesList();
    };
    InventoryTypeListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'inventory-type-list',
            templateUrl: '/app/views/inventory-type/list.html'
        }), 
        __metadata('design:paramtypes', [inventory_type_service_1.InventoryTypeService, router_1.Router])
    ], InventoryTypeListComponent);
    return InventoryTypeListComponent;
}());
exports.InventoryTypeListComponent = InventoryTypeListComponent;
//# sourceMappingURL=inventory-type-list.component.js.map