"use strict";
var InventoryType = (function () {
    function InventoryType(id, title) {
        this.id = id;
        this.title = title;
    }
    return InventoryType;
}());
exports.InventoryType = InventoryType;
//# sourceMappingURL=inventory-type.model.js.map