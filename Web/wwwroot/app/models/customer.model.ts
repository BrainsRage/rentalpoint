﻿export class Customer implements IBaseModel {
    constructor(
        public id: number,
        public name: string,
        public surname: string,
        public patronymic: string,
        public phone: string
    ) {
    }
}