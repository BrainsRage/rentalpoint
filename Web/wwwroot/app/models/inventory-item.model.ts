﻿export class InventoryItem implements IBaseModel {
    constructor(
        public id: number,
        public title: string,
        public price: number,
        public inventoryTypeId: number
    ) {
    }
}