"use strict";
var InventoryItem = (function () {
    function InventoryItem(id, title, price, inventoryTypeId) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.inventoryTypeId = inventoryTypeId;
    }
    return InventoryItem;
}());
exports.InventoryItem = InventoryItem;
//# sourceMappingURL=inventory-item.model.js.map