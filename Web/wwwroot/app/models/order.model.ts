﻿export class Order implements IBaseModel {
    constructor(
        public id: number,
        public amount: number,
        public customerId: number,
        public items: string
    ) {
    }
}