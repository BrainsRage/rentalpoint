"use strict";
var Order = (function () {
    function Order(id, amount, customerId, items) {
        this.id = id;
        this.amount = amount;
        this.customerId = customerId;
        this.items = items;
    }
    return Order;
}());
exports.Order = Order;
//# sourceMappingURL=order.model.js.map