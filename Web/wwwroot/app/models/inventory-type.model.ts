﻿export class InventoryType implements IBaseModel {
    constructor(
        public id: number,
        public title: string
    ) {
    }
}