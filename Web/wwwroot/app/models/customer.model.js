"use strict";
var Customer = (function () {
    function Customer(id, name, surname, patronymic, phone) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.phone = phone;
    }
    return Customer;
}());
exports.Customer = Customer;
//# sourceMappingURL=customer.model.js.map