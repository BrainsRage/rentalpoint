﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { InventoryItem } from '../models/inventory-item.model';
import {Observable} from 'rxjs/Rx';
import {HttpService} from './http.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class InventoryItemService extends HttpService<InventoryItem> {
    constructor(
        http: Http
    ) {
        super(http);
    }

    protected apiUrl = '/api/inventoryitem';

}