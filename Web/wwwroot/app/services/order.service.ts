﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Order } from '../models/order.model';
import {Observable} from 'rxjs/Rx';
import {HttpService} from './http.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class OrderService extends HttpService<Order> {
    constructor(
        http: Http
    ) {
        super(http);
    }

    protected apiUrl = '/api/order';
}