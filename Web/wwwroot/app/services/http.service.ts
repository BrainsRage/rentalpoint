﻿import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { InventoryType } from '../models/inventory-type.model';

import 'rxjs/add/operator/toPromise';
@Injectable()
export abstract class HttpService<T extends IBaseModel> {
    constructor(private http: Http) {}

    private headers = new Headers({ 'Content-Type': 'application/json' });
    protected apiUrl:string;

    getAll(): Promise<T[]> {
        return this.http
            .get(this.apiUrl).toPromise()
            .then(response => response.json() as T[])
            .catch(this.handleError);
    }

    getOne(id: number): Promise<T> {
        return this.getAll()
            .then(items => items.find(item => item.id === id));
    }

    delete(id: number): Promise<void> {
        const url = `${this.apiUrl}/${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
    create(item:T): Promise<T> {
        return this.http
            .post(this.apiUrl, JSON.stringify(item), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    update(item: T): Promise<T> {
        const url = `${this.apiUrl}/${item.id}`;
        return this.http
            .put(url, JSON.stringify(item), { headers: this.headers })
            .toPromise()
            .then(() => item)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); 
        return Promise.reject(error.message || error);
    }
}