﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Customer } from '../models/customer.model';
import {Observable} from 'rxjs/Rx';
import {HttpService} from './http.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class CustomerService extends HttpService<Customer> {
    constructor(
        http: Http
    ) {
        super(http);
    }

    protected apiUrl = '/api/customer';

}