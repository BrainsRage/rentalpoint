﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { InventoryType } from '../models/inventory-type.model';
import {Observable} from 'rxjs/Rx';
import {HttpService} from './http.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class InventoryTypeService extends HttpService<InventoryType> {
    constructor(
        http: Http
    ) {
        super(http);
    }

    protected apiUrl = '/api/inventorytype';

}