﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InventoryItem } from '../models/inventory-item.model';
import { InventoryType } from '../models/inventory-type.model';
import { InventoryTypeService } from '../services/inventory-type.service';
import { InventoryItemService } from '../services/inventory-item.service';

@Component({
    moduleId: module.id,
    selector: 'inventory-item-list',
    templateUrl: '/app/views/inventory-item/list.html'
})
export class InventoryItemListComponent {
    itemsList: InventoryItem[];
    typesList: InventoryType[];

    constructor(
        private itemsService: InventoryItemService,
        private typesService: InventoryTypeService,
        private router: Router
    ) {
    }

    getItemsList(): void {
        this.itemsService.getAll()
            .then(itemsList => {
                this.itemsList = itemsList;
                console.log(itemsList);
            });
    }

    getTypesList(): void {
        this.typesService.getAll()
            .then(typesList => {
                this.typesList = typesList;
                console.log(typesList);
            });
    }

    add(title: string, price: number, typeId: number): void {
        title = title.trim();
        if (!title || !price || !typeId) {
            alert('Не все поля заполнены');
            return;
        }
        var type = new InventoryItem(0, title, price, typeId);
        this.itemsService.create(type)
            .then((type) => {
                this.itemsList.push((type));
            });
    }

    delete(type: InventoryItem): void {
        this.itemsService
            .delete(type.id)
            .then(() => {
                this.itemsList = this.itemsList.filter(h => h !== type);
            });
    }

    getItemType(item: InventoryItem) {
        return this.typesList.filter(l => l.id === item.inventoryTypeId)[0].title;
    }

    ngOnInit(): void {
        this.getItemsList();
        this.getTypesList();
    }
}