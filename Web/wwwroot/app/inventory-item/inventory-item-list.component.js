"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var inventory_item_model_1 = require('../models/inventory-item.model');
var inventory_type_service_1 = require('../services/inventory-type.service');
var inventory_item_service_1 = require('../services/inventory-item.service');
var InventoryItemListComponent = (function () {
    function InventoryItemListComponent(itemsService, typesService, router) {
        this.itemsService = itemsService;
        this.typesService = typesService;
        this.router = router;
    }
    InventoryItemListComponent.prototype.getItemsList = function () {
        var _this = this;
        this.itemsService.getAll()
            .then(function (itemsList) {
            _this.itemsList = itemsList;
            console.log(itemsList);
        });
    };
    InventoryItemListComponent.prototype.getTypesList = function () {
        var _this = this;
        this.typesService.getAll()
            .then(function (typesList) {
            _this.typesList = typesList;
            console.log(typesList);
        });
    };
    InventoryItemListComponent.prototype.add = function (title, price, typeId) {
        var _this = this;
        title = title.trim();
        if (!title || !price || !typeId) {
            alert('Не все поля заполнены');
            return;
        }
        var type = new inventory_item_model_1.InventoryItem(0, title, price, typeId);
        this.itemsService.create(type)
            .then(function (type) {
            _this.itemsList.push((type));
        });
    };
    InventoryItemListComponent.prototype.delete = function (type) {
        var _this = this;
        this.itemsService
            .delete(type.id)
            .then(function () {
            _this.itemsList = _this.itemsList.filter(function (h) { return h !== type; });
        });
    };
    InventoryItemListComponent.prototype.getItemType = function (item) {
        return this.typesList.filter(function (l) { return l.id === item.inventoryTypeId; })[0].title;
    };
    InventoryItemListComponent.prototype.ngOnInit = function () {
        this.getItemsList();
        this.getTypesList();
    };
    InventoryItemListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'inventory-item-list',
            templateUrl: '/app/views/inventory-item/list.html'
        }), 
        __metadata('design:paramtypes', [inventory_item_service_1.InventoryItemService, inventory_type_service_1.InventoryTypeService, router_1.Router])
    ], InventoryItemListComponent);
    return InventoryItemListComponent;
}());
exports.InventoryItemListComponent = InventoryItemListComponent;
//# sourceMappingURL=inventory-item-list.component.js.map