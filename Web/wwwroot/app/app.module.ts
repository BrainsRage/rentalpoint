﻿import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.components";
import { RouterModule } from "@angular/router";
import { HttpModule } from "@angular/http";
import {routing} from "./app.routes";
import {CustomerListComponent} from "./customer/customer-list.component";
import {InventoryTypeListComponent} from "./inventory-type/inventory-type-list.component";
import {InventoryItemListComponent} from "./inventory-item/inventory-item-list.component";
import {OrderListComponent} from "./order/order-list.component";
import {HomeComponent} from "./home/home.component";
import {InventoryTypeService} from "./services/inventory-type.service";
import {InventoryItemService} from "./services/inventory-item.service";
import {CustomerService} from "./services/customer.service";
import {OrderService} from "./services/order.service";

@NgModule({
    declarations: [   
        AppComponent,
        InventoryTypeListComponent,
        HomeComponent,
        CustomerListComponent,
        InventoryItemListComponent,
        OrderListComponent    
    ],
    imports: [  
        BrowserModule,
        RouterModule,
        HttpModule,        
        routing
    ],
    providers: [
        InventoryTypeService,
        InventoryItemService,
        CustomerService, 
        OrderService
    ],
    bootstrap:    [AppComponent]
})
export class AppModule {}