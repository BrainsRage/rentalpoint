"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_components_1 = require("./app.components");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var app_routes_1 = require("./app.routes");
var customer_list_component_1 = require("./customer/customer-list.component");
var inventory_type_list_component_1 = require("./inventory-type/inventory-type-list.component");
var inventory_item_list_component_1 = require("./inventory-item/inventory-item-list.component");
var order_list_component_1 = require("./order/order-list.component");
var home_component_1 = require("./home/home.component");
var inventory_type_service_1 = require("./services/inventory-type.service");
var inventory_item_service_1 = require("./services/inventory-item.service");
var customer_service_1 = require("./services/customer.service");
var order_service_1 = require("./services/order.service");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_components_1.AppComponent,
                inventory_type_list_component_1.InventoryTypeListComponent,
                home_component_1.HomeComponent,
                customer_list_component_1.CustomerListComponent,
                inventory_item_list_component_1.InventoryItemListComponent,
                order_list_component_1.OrderListComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                router_1.RouterModule,
                http_1.HttpModule,
                app_routes_1.routing
            ],
            providers: [
                inventory_type_service_1.InventoryTypeService,
                inventory_item_service_1.InventoryItemService,
                customer_service_1.CustomerService,
                order_service_1.OrderService
            ],
            bootstrap: [app_components_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map