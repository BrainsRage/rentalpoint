﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InventoryItem } from '../models/inventory-item.model';
import { InventoryType } from '../models/inventory-type.model';
import { Customer } from '../models/customer.model';
import { Order } from '../models/order.model';
import { InventoryTypeService } from '../services/inventory-type.service';
import { InventoryItemService } from '../services/inventory-item.service';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';

@Component({
    moduleId: module.id,
    selector: 'order-list',
    templateUrl: '/app/views/order/list.html'
})
export class OrderListComponent {
    selectedItems: InventoryItem[]=[];
    itemsList: InventoryItem[];
    typesList: InventoryType[];
    customerList: Customer[];
    orderList: Order[];

    constructor(
        private orderService: OrderService,
        private itemsService: InventoryItemService,
        private customerService: CustomerService,
        private typesService: InventoryTypeService,
        private router: Router
    ) {
    }

    private getItemsList(): void {
        this.itemsService.getAll()
            .then(itemsList => {
                this.itemsList = itemsList;
                console.log(itemsList);
            });
    }

    private getOrdersList(): void {
        this.orderService.getAll()
            .then(ordersList => {
                this.orderList = ordersList;
                console.log(ordersList);
            });
    }

    private getCustonerList(): void {
        this.customerService.getAll()
            .then(customerList => {
                this.customerList = customerList;

                console.log(customerList);
            });
    }

    /*
    getTypesList(): void {
        this.typesService.getAll()
            .then(typesList => {
                this.typesList = typesList;
                console.log(typesList);
            });
    }
*/
    add(customerId:number): void {
        if (!customerId || this.selectedItems.length === 0) {
            alert('Не все поля заполнены');
            return;
        }
        var itemsJson = String(JSON.stringify(this.selectedItems));
        var order = new Order(
            0,
            this.calculateAmount(this.selectedItems),
            +customerId,
            itemsJson
        );
        this.orderService.create(order)
            .then((order) => {
                this.orderList.push((order));
            });
    }

    private calculateAmount(items: InventoryItem[]) {
        var amount = 0;
        items.forEach(item => {
            amount += item.price;
        });
        return amount;
    }

    delete(order: Order): void {
        this.orderService
            .delete(order.id)
            .then(() => {
                this.orderList = this.orderList.filter(h => h !== order);
            });
    }

    getItemType(item: InventoryItem) {
        return this.typesList.filter(l => l.id === item.inventoryTypeId)[0].title;
    }

    removeSelectedItem(item: InventoryItem) {
        //this.selectedItems = this.selectedItems.filter(i => i !== item);
        var index = this.selectedItems.indexOf(item);
        this.selectedItems.splice(index, 1);
    }

    addSelectedItem(itemId: number) {
        return this.selectedItems.push(this.itemsList.filter(i => i.id === +itemId)[0]);
    }

    ngOnInit(): void {
        this.getItemsList();
        this.getOrdersList();
        this.getCustonerList();
    }
}