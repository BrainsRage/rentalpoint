"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var order_model_1 = require('../models/order.model');
var inventory_type_service_1 = require('../services/inventory-type.service');
var inventory_item_service_1 = require('../services/inventory-item.service');
var customer_service_1 = require('../services/customer.service');
var order_service_1 = require('../services/order.service');
var OrderListComponent = (function () {
    function OrderListComponent(orderService, itemsService, customerService, typesService, router) {
        this.orderService = orderService;
        this.itemsService = itemsService;
        this.customerService = customerService;
        this.typesService = typesService;
        this.router = router;
        this.selectedItems = [];
    }
    OrderListComponent.prototype.getItemsList = function () {
        var _this = this;
        this.itemsService.getAll()
            .then(function (itemsList) {
            _this.itemsList = itemsList;
            console.log(itemsList);
        });
    };
    OrderListComponent.prototype.getOrdersList = function () {
        var _this = this;
        this.orderService.getAll()
            .then(function (ordersList) {
            _this.orderList = ordersList;
            console.log(ordersList);
        });
    };
    OrderListComponent.prototype.getCustonerList = function () {
        var _this = this;
        this.customerService.getAll()
            .then(function (customerList) {
            _this.customerList = customerList;
            console.log(customerList);
        });
    };
    /*
    getTypesList(): void {
        this.typesService.getAll()
            .then(typesList => {
                this.typesList = typesList;
                console.log(typesList);
            });
    }
*/
    OrderListComponent.prototype.add = function (customerId) {
        var _this = this;
        if (!customerId || this.selectedItems.length === 0) {
            alert('Не все поля заполнены');
            return;
        }
        var itemsJson = String(JSON.stringify(this.selectedItems));
        var order = new order_model_1.Order(0, this.calculateAmount(this.selectedItems), +customerId, itemsJson);
        this.orderService.create(order)
            .then(function (order) {
            _this.orderList.push((order));
        });
    };
    OrderListComponent.prototype.calculateAmount = function (items) {
        var amount = 0;
        items.forEach(function (item) {
            amount += item.price;
        });
        return amount;
    };
    OrderListComponent.prototype.delete = function (order) {
        var _this = this;
        this.orderService
            .delete(order.id)
            .then(function () {
            _this.orderList = _this.orderList.filter(function (h) { return h !== order; });
        });
    };
    OrderListComponent.prototype.getItemType = function (item) {
        return this.typesList.filter(function (l) { return l.id === item.inventoryTypeId; })[0].title;
    };
    OrderListComponent.prototype.removeSelectedItem = function (item) {
        //this.selectedItems = this.selectedItems.filter(i => i !== item);
        var index = this.selectedItems.indexOf(item);
        this.selectedItems.splice(index, 1);
    };
    OrderListComponent.prototype.addSelectedItem = function (itemId) {
        return this.selectedItems.push(this.itemsList.filter(function (i) { return i.id === +itemId; })[0]);
    };
    OrderListComponent.prototype.ngOnInit = function () {
        this.getItemsList();
        this.getOrdersList();
        this.getCustonerList();
    };
    OrderListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'order-list',
            templateUrl: '/app/views/order/list.html'
        }), 
        __metadata('design:paramtypes', [order_service_1.OrderService, inventory_item_service_1.InventoryItemService, customer_service_1.CustomerService, inventory_type_service_1.InventoryTypeService, router_1.Router])
    ], OrderListComponent);
    return OrderListComponent;
}());
exports.OrderListComponent = OrderListComponent;
//# sourceMappingURL=order-list.component.js.map