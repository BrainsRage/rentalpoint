"use strict";
var router_1 = require("@angular/router");
var inventory_type_list_component_1 = require("./inventory-type/inventory-type-list.component");
var inventory_item_list_component_1 = require("./inventory-item/inventory-item-list.component");
var home_component_1 = require("./home/home.component");
var customer_list_component_1 = require("./customer/customer-list.component");
var order_list_component_1 = require("./order/order-list.component");
var appRoutes = [
    { path: "", component: home_component_1.HomeComponent, pathMatch: "full" },
    { path: "inventory-types", component: inventory_type_list_component_1.InventoryTypeListComponent },
    { path: "inventory-items", component: inventory_item_list_component_1.InventoryItemListComponent },
    { path: "customers", component: customer_list_component_1.CustomerListComponent },
    { path: "orders", component: order_list_component_1.OrderListComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routes.js.map